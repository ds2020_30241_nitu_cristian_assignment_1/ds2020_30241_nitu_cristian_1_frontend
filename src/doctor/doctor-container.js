import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import * as API_PATIENTS from "../patient/api/patient-api";
import * as API_CAREGIVERS from "../caregiver/api/caregiver-api";
import * as API_MEDICATIONS from "../medication/api/medication-api";
import PatientTable from "../patient/components/patient-table";
import PatientForm from "../patient/components/patient-form";
import CaregiverTable from "../caregiver/components/caregiver-table";
import CaregiverForm from "../caregiver/components/caregiver-form";
import MedicationTable from "../medication/components/medication-table";
import MedicationForm from "../medication/components/medication-form";

import CaregiverNames from "../caregiver/components/caregiver-names";
import {Redirect} from "react-router-dom";
import MedicationPlanComponent from "../medicationPlan/components/medication-plan";
import PatientGiveNames from "../patient/components/patient-give-names";

class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.togglePatientForm = this.togglePatientForm.bind(this);
        this.reloadPatient = this.reloadPatient.bind(this);
        this.fetchPatients = this.fetchPatients.bind(this);

        this.toggleCaregiverForm = this.toggleCaregiverForm.bind(this);
        this.reloadCaregiver = this.reloadCaregiver.bind(this);
        this.fetchCaregivers = this.fetchCaregivers.bind(this);

        this.toggleMedicationForm = this.toggleMedicationForm.bind(this);
        this.reloadMedication = this.reloadMedication.bind(this);
        this.fetchMedications = this.fetchMedications.bind(this);

        this.addPatientToCaregiverFunc = this.addPatientToCaregiverFunc.bind(this);
        this.logOut = this.logOut.bind(this);

        this.state = {
            selectedPatient: false,
            collapseFormPatient: false,
            tableDataPatient: [],
            isLoadedPatient: false,

            selectedCaregiver: false,
            collapseFormCaregiver: false,
            tableDataCaregiver: [],
            isLoadedCaregiver: false,

            selectedMedication: false,
            collapseFormMedication: false,
            tableDataMedication: [],
            isLoadedMedication: false,

            selectedCaregiverFromList: "",
            selectedPatientFromList: "",

            errorStatus: 0,
            error: null,
            loggedOut: false

        };
    }

    togglePatientForm() {
        this.setState({selectedPatient: !this.state.selectedPatient});
    }

    logOut() {
        this.setState({
            loggedOut: true,
        });
        localStorage.setItem('user', JSON.stringify(''));
    };

    addPatientToCaregiverFunc() {
        this.setState({selectedCaregiverFromList: CaregiverNames.selectedValue,
                            selectedPatientFromList: PatientGiveNames.selectedValue});

            return API_CAREGIVERS.addPatientToCaregiver(PatientGiveNames.selectedValue,
                CaregiverNames.selectedValue,
                (result, status, error) => {
                    if (result !== null && (status === 200 || status === 201)) {
                        console.log("Successfully inserted caregiver to patient with id: " + result);
                        this.reloadHandler();
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: error
                        }));
                    }
              });


    }

    toggleCaregiverForm() {
        this.setState({selectedCaregiver: !this.state.selectedCaregiver});
    }

    toggleMedicationForm() {
        this.setState({selectedMedication: !this.state.selectedMedication});
    }


    componentDidMount() {
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedications();
    }


    reloadPatient() {
        this.setState({
            isLoadedPatient: false
        });
        this.togglePatientForm();
        this.fetchPatients();
    }

    reloadCaregiver() {
        this.setState({
            isLoadedCaregiver: false
        });
        this.toggleCaregiverForm();
        this.fetchCaregivers();
    }

    reloadMedication() {
        this.setState({
            isLoadedMedication: false
        });
        this.toggleMedicationForm();
        this.fetchMedications();
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataPatient: result,
                    isLoadedPatient: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregivers() {
        return API_CAREGIVERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataCaregiver: result,
                    isLoadedCaregiver: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedications() {
        return API_MEDICATIONS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataMedication: result,
                    isLoadedMedication: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Doctor management </strong>
                </CardHeader>
                <Card>

                    <br/>
                    <Row>
                        <Col>
                            <Button id = "d" color="primary"  onClick={this.logOut}>Logout </Button>

                            {this.state.loggedOut === true &&
                            <Redirect
                                to={{
                                    pathname: "/login",
                                    state: {
                                        error: "Error",
                                    },
                                }}
                            />
                            }
                        </Col>
                    </Row>


                    <br/>
                    <h5> Patient Operations </h5>

                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.togglePatientForm}>Add Patient </Button>
                        </Col>
                        <Col>
                            <CaregiverNames>   </CaregiverNames>
                            <PatientGiveNames>   </PatientGiveNames>

                            <Button color="primary" onClick={this.addPatientToCaregiverFunc}>
                                Add Patient to Caregiver
                            </Button>
                        </Col>

                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedPatient && <PatientTable tableDataPatient = {this.state.tableDataPatient}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                    <br/>
                    <br/>
                    <h5> Caregiver Operations </h5>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleCaregiverForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedCaregiver &&
                            <CaregiverTable tableDataCaregiver = {this.state.tableDataCaregiver}/>}

                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>

                    <br/>
                    <h5> Medication Operations </h5>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleMedicationForm}>Add Medication in system </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedMedication &&
                            <MedicationTable tableDataMedication = {this.state.tableDataMedication}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                    <br/>

                    <br/>
                    <h5> Medication Plan </h5>
                    <MedicationPlanComponent></MedicationPlanComponent>

                </Card>

                <Modal isOpen={this.state.selectedPatient} togglePatient={this.togglePatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader togglePatient={this.togglePatientForm}> Add Patient </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reloadPatient}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedCaregiver} toggleCaregiver={this.toggleCaregiverForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggleCaregiver={this.toggleCaregiverForm}> Add Caregiver </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reloadCaregiver}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedMedication} toggleMedication={this.toggleMedicationForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggleMedication={this.toggleMedicationForm}> Add Medication </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reloadMedication}/>
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}

export default DoctorContainer;
