import {HOST} from '../../commons/hosts';
import RestApiClient from '../../commons/api/rest-client';

const endpoint = {
    doctor: '/doctor'
};

function getDoctorById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.doctor + '/' +  id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
   getDoctorById
};
