import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medicationPlan: '/medicationPlan'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(medicationPlan, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function addMedicationToMedicationPlan(medicationPlanId, medicationName, dailyInterval, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + '/addMedicationLink/' +
        medicationPlanId + '/' + JSON.parse(JSON.stringify(medicationName)) , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: dailyInterval
    });

    console.log((JSON.stringify(dailyInterval)));
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    postMedicationPlan,
    addMedicationToMedicationPlan
};
