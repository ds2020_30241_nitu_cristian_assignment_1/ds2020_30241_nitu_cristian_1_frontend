import React from 'react';
import {
    Button,
    Col,
    Row
} from 'reactstrap';
import * as API_MEDICATION_PLAN from "../../medicationPlan/api/medication-plan-api";
import MedicationNames from "../../medication/components/medication-names";
import * as API_PATIENTS from "../../patient/api/patient-api";
import {Modal} from "react-bootstrap";
import moment from "moment";
import PatientNamesPlan from "../../patient/components/patient-names-plan";


class MedicationPlanComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            idMedicationPlan: '',
            startTreatment: '',
            endTreatment: '',
            selectedPatientFromList: '',
            show: false,
            dailyInterval: '',
            selectedMedicationFromList: ''
        };

        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.createMedicationPlan = this.createMedicationPlan.bind(this);
        this.addPlanForPatient = this.addPlanForPatient.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.hideModal = this.hideModal.bind(this);
        this.showModal = this.showModal.bind(this);
        this.handleDailyIntervalChange = this.handleDailyIntervalChange.bind(this);
        this.addMedicationToPlan = this.addMedicationToPlan.bind(this);
    }

    showModal = () => {
        this.setState({ show: true });
    };


    hideModal = () => {
        this.setState({ show: false });
    };

    handleStartDateChange(evt) {
        this.setState({
            startTreatment: evt.target.value,
        });
    };

    handleEndDateChange(evt) {
        this.setState({
            endTreatment: evt.target.value,
        });
    };

    handleDailyIntervalChange(evt) {
        this.setState({
            dailyInterval: evt.target.value,
        });
    };

    addMedicationToPlan(){
        this.setState({selectedMedicationFromList: MedicationNames.selectedValue});

        if(MedicationNames.selectedValue === ''){
            alert("Select a medication");
        }
        else if(this.state.dailyInterval === ''){
            alert("Type a daily interval for the medication");
        }

        else {
             return API_MEDICATION_PLAN.addMedicationToMedicationPlan(this.state.idMedicationPlan,
                MedicationNames.selectedValue, this.state.dailyInterval,
                (result, status, error) => {
                    if (result !== null && (status === 200 || status === 201)) {
                        console.log("Successfully added medication to medication plan with id: " + result);
                        alert("Medication was added successfully");
                        this.setState({
                            dailyInterval: ''
                        })
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: error
                        }));
                    }
                });
        }
    }

    createMedicationPlan() {
        let medicationPlan = {
            startTreatment: this.state.startTreatment,
            endTreatment: this.state.endTreatment,
        };

        if(!moment(medicationPlan.startTreatment).isValid()){
            alert("Enter a valid date for the beginning of the treatment");
        }

        else if(!moment(medicationPlan.endTreatment).isValid()){
            alert("Enter a valid date for the end of the treatment");
        }

        else if(moment(medicationPlan.startTreatment).isAfter(moment(medicationPlan.endTreatment))){
            alert("Start date cannot be after end date");
        }

        else if(PatientNamesPlan.selectedValue === ''){
            alert("You must select a patient");
        }

        else {
            this.showModal();
            return API_MEDICATION_PLAN.postMedicationPlan(medicationPlan, (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully added medication plan with id: " + result);
                    this.setState({
                        idMedicationPlan: result,
                        isModalOpen: true,
                        startTreatment: '',
                        endTreatment: ''
                    })

                    this.addPlanForPatient(PatientNamesPlan.selectedValue, result);


                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });
        }
    }

    addPlanForPatient(patient, idPlan){
        return API_PATIENTS.addMedicationPlanToPatient(patient, idPlan,
            (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully added medication plan to patient with id: " + result);
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });

    }

    render() {
        return (
            <div>

                <Row>
                    <Col sm={{size: '8'}}>
                        <label>Treatment start date</label>
                        <input type="text" value={this.state.startTreatment} onChange={this.handleStartDateChange} />
                    </Col>
                    <Col sm={{size: '8'}}>
                        <label>Treatment end date</label>
                        <input type="text" value={this.state.endTreatment} onChange={this.handleEndDateChange} />
                        <Button id = "a" color="primary"  onClick={this.createMedicationPlan}>Create medication plan</Button>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <PatientNamesPlan></PatientNamesPlan>
                        <Modal show={this.state.show}>
                            <MedicationNames></MedicationNames>
                            <br/>
                            <label>Daily interval</label>
                            <input type="text" value={this.state.dailyInterval} onChange={this.handleDailyIntervalChange} />
                            <br/>
                            <Button onClick={this.addMedicationToPlan}>Add medication</Button>
                            <br/>
                            <Button onClick={this.hideModal}>Close</Button>
                        </Modal>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default MedicationPlanComponent;