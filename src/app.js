import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import DoctorContainer from "./doctor/doctor-container";

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import LoginContainer from "./login/login-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import {HOST} from "./commons/hosts";

import { Client } from '@stomp/stompjs';
import $ from 'jquery';
window.$ = $;


class App extends React.Component {

    static caregiverId = '';
    constructor() {

        if(!(localStorage.getItem('user'))) {
            localStorage.setItem('user', JSON.stringify(''));
        }

        if(!(localStorage.getItem('stateForCaregiver'))) {
            localStorage.setItem('stateForCaregiver', JSON.stringify(''));
        }

        else {

        }

        super();
        this.state = {

            'role': JSON.parse(localStorage.getItem('user')).role,
            'cr': JSON.parse(localStorage.getItem('user')).userId
        };
    }

    componentDidMount() {

        console.log('Component did mount');
        this.client = new Client();

        this.client.configure({
            brokerURL: 'wss://nitu-cristian-medical-backend.herokuapp.com/stomp',
            onConnect: () => {
                console.log('onConnect');

                this.client.subscribe('/topic/violations', message => {
                    if(JSON.parse(message.body).msg)
                    $("#greetings").append("<tr><td>" + JSON.parse(message.body).msg + " " + JSON.parse(message.body).patientUsername
                        + " (details: start " + JSON.parse(message.body).startActivity
                        + "; end " + JSON.parse(message.body).endActivity + "; activity "
                        + JSON.parse(message.body).activity + ")" + "</td></tr>");

                        localStorage.setItem('stateForCaregiver', JSON.stringify(JSON.parse(message.body).caregiverId));

                });
            },

        });

        this.client.activate();
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        this.setState({
            role: JSON.parse(localStorage.getItem('user')).role,
            cr: JSON.parse(localStorage.getItem('user')).userId
        });

    }

    render() {
        return (
            <div className={styles.back}>

                <Router>

                    <div>
                        <NavigationBar />
                        <Switch>


                            <Route
                                exact
                                path='/'
                                render={() => <Home/>}
                            />


                            {this.state.role === 'DOCTOR' &&
                            <Route
                                exact
                                path='/doctor'
                                render={() => <DoctorContainer/>}
                            />
                            }


                            {this.state.role === 'PATIENT' &&
                            <Route
                                exact
                                path='/patient'
                                render={() => <PatientContainer/>}
                            />
                            }


                            <Route
                                exact
                                path='/login'
                                render={() => <LoginContainer/>}
                            />


                            {this.state.role === 'CAREGIVER' &&
                            <Route
                                exact
                                path='/caregiver'
                                render={() => <CaregiverContainer/>}
                            />



                            }

                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() =><ErrorPage/>} />
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
