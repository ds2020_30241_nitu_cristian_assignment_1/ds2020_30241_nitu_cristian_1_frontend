import React from 'react';
import {
    Button,
} from 'reactstrap';
import * as API_CAREGIVER from '../caregiver/api/caregiver-api';
import LoginComponent from '../login/component/login-component';
import {Redirect} from 'react-router-dom';


class CaregiverContainer extends React.Component {
    caregiverId = LoginComponent.idUser;

    constructor(props) {
        super(props);

        this.state = {

            isListEnabled: false,
            isPatientListEnabled: false,
            caregiver: JSON.parse(localStorage.getItem('caregiver')),
            errorStatus: 0,
            error: null,
            loggedOut: false
        };

        this.getCaregiver = this.getCaregiver.bind(this);
        this.toggleList = this.toggleList.bind(this);
        this.togglePatientList = this.togglePatientList(this);
        this.logOut = this.logOut.bind(this);
    }

    toggleList() {
        this.setState({isListEnabled: !this.state.isListEnabled});
    }

    togglePatientList() {
        this.setState({isPatientListEnabled: !this.state.isPatientListEnabled});
    }

    getCaregiver(caregiverId) {
        caregiverId = LoginComponent.idUser;
        return API_CAREGIVER.getCaregiverById(caregiverId, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    caregiver: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
            console.log(this.state.caregiver);
        });
    }

    logOut() {
        this.setState({
            loggedOut: true,
        });
        localStorage.setItem('user', JSON.stringify(''));
    };

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <table id="conversation" className="table table-striped">
                            <thead>
                            <tr>
                                <th>Violated rules</th>
                            </tr>
                            </thead>
                            { this.state.caregiver.caregiverId === JSON.parse(localStorage.getItem('stateForCaregiver'))
                            && <tbody id="greetings">
                            </tbody> }
                        </table>
                    </div>
                </div>
                <br/>
                <Button color="primary" onClick={this.toggleList}>Caregiver details</Button>
                {this.state.isListEnabled &&
                <ul>
                    <li>
                        Username: {this.state.caregiver.username}
                    </li>
                    <li>
                        Password: {this.state.caregiver.password}
                    </li>
                    <li>
                        Name: {this.state.caregiver.name}
                    </li>
                    <li>
                        Birthdate: {this.state.caregiver.birthdate}
                    </li>
                    <li>
                        Gender: {this.state.caregiver.gender}
                    </li>
                    <li>
                        Address: {this.state.caregiver.address}
                    </li>

                    <li>
                        Associated Patients:
                        <ul>
                            {this.state.caregiver.patientDTOSet.map(item => (
                                <li key={item.patientId}>
                                    <div>Patient name: {item.name}</div>
                                    <div>Patient username: {item.username}</div>
                                    <div>Patient gender: {item.gender}</div>
                                    <div>Patient birthdate: {item.birthdate}</div>
                                    <div>Patient medical record: {item.medicalRecord}</div>
                                    <div>Medication plans:
                                        <ul>
                                            {item.medicationPlanDTOSet.map(item3 => (
                                                <li key={item3.idMedicationPlan}>
                                                    <div>Medication plan start date: {item3.startTreatment}</div>
                                                    <div>Medication plan end date: {item3.endTreatment}</div>

                                                    {
                                                        item3.medicationPlanToMedicationDTOSet.map(item4 => (
                                                            <ul>
                                                                <li>
                                                                    <div>Name: {item4.medicationName}</div>
                                                                    <div>DailyInterval: {item4.dailyInterval}</div>
                                                                </li>
                                                            </ul>
                                                        ))
                                                    }

                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </li>

                </ul>
                }


                <Button id = "d" color="primary"  onClick={this.logOut}>Logout </Button>

                {this.state.loggedOut === true &&
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {
                            error: "Error",
                        },
                    }}
                />
                }
            </div>
        )

    }
}

export default CaregiverContainer;
