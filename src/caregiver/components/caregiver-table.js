import React from 'react';
import Table from '../../commons/tables/table';
import * as API_CAREGIVERS from '../api/caregiver-api';
import APIResponseErrorMessage from '../../commons/errorhandling/api-response-error-message';
import {Modal, ModalBody, ModalHeader} from 'reactstrap';
import CaregiverFormUpdate from './caregiver-form-update';


class CaregiverTable extends React.Component {

    static idCaregiver;

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Delete',
            id: 'button',
            accessor: 'caregiverId',
            Cell: ({value}) => (
                <button onClick={()=>{this.deleteCaregiverById(value)}}>Delete</button>
            ),
        },
        {
            Header: 'Update',
            id: 'button2',
            accessor: 'caregiverId',

            Cell: ({value}) => (
                <button onClick={()=>{this.toggleForm(); CaregiverTable.idCaregiver = value; }}>Update</button>
            ),
        }

    ];

    sub_columns = this.columns.slice(0);


    filters = [
        {
            accessor: 'name',
        }
    ];


    constructor(props) {
        super(props);

        this.deleteCaregiverById = this.deleteCaregiverById.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selected: false,
            tableData: this.props.tableData,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    fetchCaregivers() {
        return API_CAREGIVERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteCaregiverById(id) {
        return API_CAREGIVERS.deleteCaregiverById(id,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({tableData: this.fetchCaregivers, isLoaded: false});
                this.fetchCaregivers();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <Table data={this.state.tableData}
                                               columns={this.sub_columns}
                                               search={this.filters}
                                               pageSize={5}
                />
                }

                {this.state.errorStatus > 0 && <APIResponseErrorMessage errorStatus={this.state.errorStatus}
                                                                        error={this.state.error}
                />
                }

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Update </ModalHeader>
                    <ModalBody>
                        <CaregiverFormUpdate reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}

export default CaregiverTable;