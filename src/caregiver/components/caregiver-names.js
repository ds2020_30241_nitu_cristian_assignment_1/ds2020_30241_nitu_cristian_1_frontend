import React from 'react';

class CaregiverNames extends React.Component {
    state = {
        caregiverList: [],
        selectedName: "",
        validationError: ""
    };

    static selectedValue;

    componentDidMount() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/caregiver/caregiverNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                caregiverList: [{value: '', display: '(Select caregiver)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    componentDidUpdate() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/caregiver/caregiverNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                caregiverList: [{value: '', display: '(Select caregiver)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    render() {
        return (
            <div>

                <select value={this.state.selectedName}
                        onChange={e =>
                            this.setState({
                                selectedName: e.target.value
                            })
                        }
                >


                    {CaregiverNames.selectedValue = this.state.selectedName}
                    {this.state.caregiverList.map(team => (
                        <option
                            key={team.value}
                            value={team.value}
                        >
                            {team.display}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

export default CaregiverNames