import {HOST} from '../../commons/hosts';
import RestApiClient from '../../commons/api/rest-client';

const endpoint = {
    caregiver: '/caregiver'
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiversNames(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/caregiverNames', {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function updateCaregiver(id, caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' +  id, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function addPatientToCaregiver(patient, caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/addPatient/' +
        JSON.parse(JSON.stringify(caregiver)) + '/'+ JSON.parse(JSON.stringify(patient)), {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
//        body: JSON.stringify(caregiver)
    });

    console.log(JSON.parse(JSON.stringify(caregiver)));
    console.log((JSON.stringify(patient)));
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' +  id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteCaregiverById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + '/' +  id, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    postCaregiver,
    getCaregiverById,
    deleteCaregiverById,
    updateCaregiver,
    getCaregiversNames,
    addPatientToCaregiver
};
