import React from "react";
import Table from "../../commons/tables/table";
import * as API_MEDICATION from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import MedicationFormUpdate from "./medication-form-update";


class MedicationTable extends React.Component {

    static idMedication;

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: 'Side Effect',
            accessor: 'sideEffect',
        },
        {
            Header: 'Delete',
            id: 'button',
            accessor: 'medicationId',
            Cell: ({value}) => (
                <button onClick={()=>{console.log('clicked value', value); this.deleteMedicationById(value)}}>Delete</button>
            ),
        },
        {
            Header: 'Update',
            id: 'button2',
            accessor: 'medicationId',

            Cell: ({value}) => (
                <button onClick={()=>{console.log('clicked value', value); this.toggleForm();
                MedicationTable.idMedication = value; }}>Update</button>
            ),
        }

    ];

    sub_columns = this.columns.slice(0);


    filters = [
        {
            accessor: 'name',
        }
    ];


    constructor(props) {
        super(props);

        this.deleteMedicationById = this.deleteMedicationById.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selected: false,
            tableData: this.props.tableData,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    fetchMedications() {
        return API_MEDICATION.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteMedicationById(id) {
        return API_MEDICATION.deleteMedicationById(id,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({tableData: this.fetchMedications, isLoaded: false});
                this.fetchMedications();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedications();
    }

    componentDidMount() {
        this.fetchMedications();
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <Table data={this.state.tableData}
                                               columns={this.sub_columns}
                                               search={this.filters}
                                               pageSize={5}
                />
                }

                {this.state.errorStatus > 0 && <APIResponseErrorMessage errorStatus={this.state.errorStatus}
                                                                        error={this.state.error}
                />
                }

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Update </ModalHeader>
                    <ModalBody>
                        <MedicationFormUpdate reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}

export default MedicationTable;