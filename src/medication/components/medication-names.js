import React from 'react';

class MedicationNames extends React.Component {
    state = {
        medicationList: [],
        selectedName: "",
        validationError: ""
    };

    static selectedValue;

    componentDidMount() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/medication/medicationNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                medicationList: [{value: '', display: '(Select medication)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    componentDidUpdate() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/medication/medicationNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                medicationList: [{value: '', display: '(Select medication)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    render() {
        return (
            <div>

                <select value={this.state.selectedName}
                        onChange={e =>
                            this.setState({
                                selectedName: e.target.value
                            })
                        }

                >

                    {MedicationNames.selectedValue = this.state.selectedName}
                    {this.state.medicationList.map(medication => (
                        <option
                            key={medication.value}
                            value={medication.value}
                        >
                            {medication.display}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

export default MedicationNames