const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const isNormalIntegerValidator = (value) => {
    return Math.floor(Number(value)) !== Infinity &&
        String(Math.floor(Number(value))) === value &&
        Math.floor(Number(value)) >= 0;
};

const requiredValidator = value => {
    return value.trim() !== '';
};

const validateMedication = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;

            case 'isNormalIntegerValidator': isValid = isValid && isNormalIntegerValidator(value);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validateMedication;
