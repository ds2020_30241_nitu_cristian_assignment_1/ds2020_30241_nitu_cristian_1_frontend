import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    login: '/user'
};

function getUserByUsernameAndPassword(username, password, callback) {
    let request = new Request(HOST.backend_api + endpoint.login + '/' +
        JSON.parse(JSON.stringify(username)) + '/'+ JSON.parse(JSON.stringify(password)), {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getUserByUsernameAndPassword
};
