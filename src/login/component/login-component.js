import React, { Component } from 'react';
import {Button, Col} from "reactstrap";
import * as API_LOGIN from "../api/login-api";


import * as API_PATIENTS from "../../patient/api/patient-api";
import * as API_CAREGIVER from "../../caregiver/api/caregiver-api";
import * as API_DOCTOR from "../../doctor/api/doctor-api";

import { history } from "../history-route";
import { Row } from "reactstrap";

class LoginComponent extends Component {
    static doctor;
    static idUser;
    static username;
    static caregiver;
    static patient;
    static user;
    static userRole;
    constructor() {
        super();
        this.state = {
            patient: '',
            username: '',
            caregiver: '',
            password: '',
            doctor: '',
            role: '',
            error: '',
            id: '',
            clicked: false
        };

        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.dismissError = this.dismissError.bind(this);
        this.logInFunction = this.logInFunction.bind(this);
    }

    dismissError() {
        this.setState({ error: '' });
    }

    getDoctor(doctorId) {
        return API_DOCTOR.getDoctorById(doctorId, (result, status, err) => {

            if (result !== null && status === 200) {
                LoginComponent.doctor = JSON.stringify(result);
                localStorage.setItem('doctor', JSON.stringify(result));
                this.setState({
                    doctor: result,
                    role: result.role
                });
                history.push('/doctor');
                history.go(0);

            } else {

            }
        });
    }


    getPatient(patientId) {
        return API_PATIENTS.getPatientById(patientId, (result, status, err) => {

            if (result !== null && status === 200) {
                localStorage.setItem('patient', JSON.stringify(result));
                LoginComponent.patient = JSON.stringify(result);
                this.setState({
                    patient: result,
                    role: result.role
                });
                history.push('/patient');
                history.go(0);
             } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getCaregiver(caregiverId) {
        return API_CAREGIVER.getCaregiverById(caregiverId, (result, status, err) => {
            if (result !== null && status === 200) {
                LoginComponent.caregiver = JSON.stringify(result);
                localStorage.setItem('caregiver', JSON.stringify(result));
                this.setState({
                    caregiver: result,
                    role: result.role
                });
                history.push('/caregiver');
                history.go(0);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    logInFunction(){
        return API_LOGIN.getUserByUsernameAndPassword(this.state.username,
            this.state.password, (result, status, err) => {

            if (result !== null && status === 200) {
                LoginComponent.idUser = result.userId;
                LoginComponent.username = result.username;

                this.setState({
                    clicked: true,
                    role: result.role
                });
                localStorage.setItem('user', JSON.stringify(result));
                LoginComponent.user = JSON.stringify(result);
                LoginComponent.userRole = result.role;
                if(result.role === 'PATIENT'){
                    this.getPatient(result.userId);
                }

                else if(result.role === 'CAREGIVER'){
                    this.getCaregiver(result.userId);
                }

                else if(result.role === 'DOCTOR'){
                    this.getDoctor(result.userId);
                }

                this.setState({
                    role: result.role,
                    id: result.userId
                });
            } else {
                
            }
        });

    }
    handleSubmit(evt) {
        evt.preventDefault();

        if (!this.state.username) {
            return this.setState({ error: 'Username is required' });
        }

        if (!this.state.password) {
            return this.setState({ error: 'Password is required' });
        }

        return this.setState({ error: '' });
    }

    handleUserChange(evt) {
        this.setState({
            username: evt.target.value,
        });
    };

    handlePassChange(evt) {
        this.setState({
            password: evt.target.value,
        });
    }

    render() {
        return (
            <div className="Login">
                <br/>
                <br/>
                <br/>
                <form onSubmit={this.handleSubmit}>
                    {
                        this.state.error &&
                        <h3 data-test="error" onClick={this.dismissError}>
                            <button onClick={this.dismissError}>✖</button>
                            {this.state.error}
                        </h3>
                    }
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                        <label>Username</label>
                        <input type="text" data-test="username" value={this.state.username} onChange={this.handleUserChange} />
                        </Col>
                    </Row>

                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                        <label>Password</label>
                        <input type="password" data-test="password" value={this.state.password} onChange={this.handlePassChange} />
                        </Col>
                    </Row>
                    <Col sm={{size: '8', offset: 2}}>
                        <Button id = "d" color="primary"  onClick={this.logInFunction}>Log in </Button>
                    </Col>
                </form>
            </div>
        );
    }
}

export default LoginComponent;