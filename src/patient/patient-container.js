import React from 'react';

import {
    Button
} from 'reactstrap';
import * as API_PATIENTS from "../patient/api/patient-api";
import LoginComponent from "../login/component/login-component";
import {Redirect} from "react-router-dom";

class PatientContainer extends React.Component {
    patientId = LoginComponent.idUser;

    constructor(props) {
        super(props);

        this.state = {
            isListEnabled: false,
            patient: JSON.parse(localStorage.getItem('patient')),
            errorStatus: 0,
            error: null,
            loggedOut: false,
            isMedicationPlanListEnabled: false
        };

        this.getPatient = this.getPatient.bind(this);
        this.toggleList = this.toggleList.bind(this);
        this.logOut = this.logOut.bind(this);
        this.toggleListMedicationPlan = this.toggleListMedicationPlan.bind(this);
    }

    toggleList() {
        this.setState({isListEnabled: !this.state.isListEnabled});
    }

    toggleListMedicationPlan() {
        this.setState({isMedicationPlanListEnabled: !this.state.isMedicationPlanListEnabled});
    }

    getPatient(patientId) {
        patientId = LoginComponent.idUser;
        alert("Dada" + patientId);
        return API_PATIENTS.getPatientById(patientId, (result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    patient: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
            console.log(this.state.patient);
        });
    }

    logOut() {
        this.setState({
            loggedOut: true,
        });
        localStorage.setItem('user', JSON.stringify(''));
    };

    componentDidMount() {
    }

    render() {
        return (
            <div>
                <br/>

                <div style={{ display: "flex" }}>
                    <Button id = "d" color="primary"  onClick={this.logOut}
                            style={{ marginLeft: "auto" }}
                    >
                        Log out
                    </Button>
                </div>


                <Button color="primary" onClick={this.toggleList}>Patient details</Button>

                {this.state.isListEnabled &&
                <ul>
                    <li>
                        Username: {this.state.patient.username}
                    </li>
                    <li>
                        Password: {this.state.patient.password}
                    </li>
                    <li>
                        Name: {this.state.patient.name}
                    </li>
                    <li>
                        Birthdate: {this.state.patient.birthdate}
                    </li>
                    <li>
                        Gender: {this.state.patient.gender}
                    </li>
                    <li>
                        Address: {this.state.patient.address}
                    </li>
                    <li>
                        Medical record: {this.state.patient.medicalRecord}
                    </li>
                </ul>
                }


                {this.state.loggedOut === true &&
                <Redirect
                    to={{
                        pathname: "/login",
                        state: {
                            error: "Error",
                        },
                    }}
                />
                }

                <br/>
                <br/>
                <br/>

                <Button id = "getMedicationPlans" color="primary"  onClick={this.toggleListMedicationPlan}>
                    Show medication plans
                </Button>

                {this.state.isMedicationPlanListEnabled &&
                <ul>
                    <li>
                        Medication plans:
                        <ul>
                            {this.state.patient.medicationPlanDTOSet.map(item => (
                                <li key={item.idMedicationPlan}>
                                    <div>Medication plan start date: {item.startTreatment}</div>
                                    <div>Medication plan end date: {item.endTreatment}</div>

                                    {
                                        item.medicationPlanToMedicationDTOSet.map(item2 => (
                                            <ul>
                                                <li>
                                                    <div>Name: {item2.medicationName}</div>
                                                    <div>DailyInterval: {item2.dailyInterval}</div>
                                                </li>
                                            </ul>
                                        ))
                                    }

                                </li>
                            ))}
                        </ul>
                    </li>
                </ul>
                }


            </div>
        )

    }
}

export default PatientContainer;
