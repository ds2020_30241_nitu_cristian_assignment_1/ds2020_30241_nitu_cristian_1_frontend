import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePatient(id, patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/' +  id, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function getPatientById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/' +  id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deletePatientById(id, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/' +  id, {
        method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function addMedicationPlanToPatient(patient, medicationPlanId, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + '/addPlan/' +
        JSON.parse(JSON.stringify(patient)) + '/'+ medicationPlanId, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log((JSON.stringify(patient)));
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getPatients,
    postPatient,
    getPatientById,
    deletePatientById,
    updatePatient,
    addMedicationPlanToPatient
};
