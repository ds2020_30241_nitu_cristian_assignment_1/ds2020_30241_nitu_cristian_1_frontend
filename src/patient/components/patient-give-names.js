import React from 'react';

class PatientGiveNames extends React.Component {
    state = {
        patList: [],
        selectedName: "",
        validationError: ""
    };

    static selectedValue;

    componentDidMount() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/patient/patientNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                patList: [{value: '', display: '(Select patient)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    componentDidUpdate() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/patient/patientNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                patList: [{value: '', display: '(Select patient)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }


    render() {
        return (
            <div>

                <select value={this.state.selectedName}
                        onChange={e =>
                            this.setState({
                                selectedName: e.target.value
                            })
                        }
                >

                    {PatientGiveNames.selectedValue = this.state.selectedName}
                    {this.state.patList.map(pat => (
                        <option
                            key={pat.value}
                            value={pat.value}
                        >
                            {pat.display}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

export default PatientGiveNames