import React from 'react';

class PatientNames extends React.Component {
    state = {
        patientList: [],
        selectedName: "",
        validationError: ""
    };

    static selectedValue;

    componentDidMount() {
        fetch(
            "https://nitu-cristian-medical-backend.herokuapp.com/patient/patientNames"
        )
            .then(response => {
                return response.json();
            }).then(data => {
            let names = data.map(name => {
                return {value: name, display: name}
            });
            this.setState({
                patientList: [{value: '', display: '(Select patient)'}].concat(names)
            });
        }).catch(error => {
            console.log(error);
        });
    }

    render() {
        return (
            <div>

                <select value={this.state.selectedName}
                        onChange={e =>
                            this.setState({
                                selectedName: e.target.value
                            })
                        }
                >

                    {PatientNames.selectedValue = this.state.selectedName}
                    {this.state.patientList.map(team => (
                        <option
                            key={team.value}
                            value={team.value}
                        >
                            {team.display}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
}

export default PatientNames