import moment from "moment";

const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const requiredValidator = value => {
    return value.trim() !== '';
};

const dateValidator = value => {
    return moment(value).isValid();
};

const genderValidator = value => {
    if((value === "male") || (value === "female")){
        return true;
    }
};

const validatePatient = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;

            case 'birthdateValidator': isValid = isValid && dateValidator(value);
                break;

            case 'genderValidator': isValid = isValid && genderValidator(value);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validatePatient;
