import React from "react";
import Table from "../../commons/tables/table";
import * as API_PATIENTS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import PatientFormUpdate from "./patient-form-update";

class PatientTable extends React.Component {

    static idF;

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Medical record',
            accessor: 'medicalRecord',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Delete',
            id: 'button',
            accessor: 'patientId',
            Cell: ({value}) => (
                <button onClick={()=>{console.log('clicked value', value); this.deletePatientById(value)}}>Delete</button>
            ),
        },
        {
            Header: 'Update',
            id: 'button2',
            accessor: 'patientId',

            Cell: ({value}) => (
                <button onClick={()=>{console.log('clicked value', value); this.toggleForm(); PatientTable.idF = value; }}>Update</button>

            ),
        }
    ];

    sub_columns = this.columns.slice(0);


    filters = [
        {
            accessor: 'name',
        }
    ];


    constructor(props) {
        super(props);

        this.deletePatientById = this.deletePatientById.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selected: false,
            tableData: this.props.tableData,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deletePatientById(id) {
        return API_PATIENTS.deletePatientById(id,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({tableData: this.fetchPatients, isLoaded: false});
                this.fetchPatients();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }

    componentDidMount() {
        this.fetchPatients();
    }

    render() {
        return (
            <div>
                {this.state.isLoaded && <Table data={this.state.tableData}
                                               columns={this.sub_columns}
                                               search={this.filters}
                                               pageSize={5}
                />
                }

                {this.state.errorStatus > 0 && <APIResponseErrorMessage errorStatus={this.state.errorStatus}
                                                                        error={this.state.error}
                />
                }

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Update </ModalHeader>
                    <ModalBody>
                        <PatientFormUpdate reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}

export default PatientTable;